import {Router} from '@angular/router';
import {Injectable} from '@angular/core';
import * as firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/app';
import 'firebase/firestore';
import {LocalStorageService} from 'ngx-webstorage';

@Injectable()
export class AuthService {
  token: string;
  email: string;
  errorMsg = false;
  admin = false;
  emailError: string;

  constructor(private router: Router,
              private localSt: LocalStorageService) {
    if (this.localSt.retrieve('token')) {
      this.token = this.localSt.retrieve('token');
      this.email = this.localSt.retrieve('email');
    }
  }

  signUpUser(email: string, password: string) {
    firebase.auth().createUserWithEmailAndPassword(email, password)
      .then(() => console.log('Successfully signed up'))
      .catch(
        error => {
        this.emailError = error.message;
        console.log(this.emailError);

        }
      );
    // this.isSignedUp = true;
  }


  singInUser(email: string, password: string) {
    firebase.auth().signInWithEmailAndPassword(email, password)
      .then(
        response => {
          this.email = response.user.email;
          this.router.navigate(['/advert-page']);
          firebase.auth().currentUser.getIdToken()
            .then((token: string) => this.token = token);
          const token = this.getToken();
          this.getToken();
          setTimeout(() => {
            this.localSt.store('token', this.token);
            this.localSt.store('email', this.email);
            console.log(this.localSt.retrieve('token')); } , 2000);
        }
      )
      .catch(
        error =>
          this.isWrongCredentials()
      );

  }

  logout() {
    firebase.auth().signOut();
    this.token = null;
    this.email = null;
    this.admin = false;
    this.localSt.clear('token');
    this.localSt.clear('email');
    this.router.navigate(['/signin']);
  }

  getToken() {

    firebase.auth().currentUser.getIdToken()
      .then(
        (token: string) => this.token = token
      );
    return this.token;

  }

  isAuthenticated() {
    return this.token != null;
  }
  isWrongCredentials() {
    return this.errorMsg = true;
  }
  isEmailExist() {
    return !!this.emailError;
  }
  isAdmin() {
    if (this.email === 'admin@admin.com') {
      this.admin = true;
    }
    return this.admin;

  }
}
