import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';

import { AppRoutingModule, routingComponents } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { CreateMainComponent } from './create-page/create-main/create-main.component';
import { CreaterightComponent } from './create-page/createright/createright.component';
import { AdvertListComponent } from './advert-page/advert-list/advert-list.component';
import { FormsModule, ReactiveFormsModule} from '@angular/forms';
import { HttpClientModule} from '@angular/common/http';
import { FirebaseService } from './firebase.service';
import { AngularFireDatabaseModule} from 'angularfire2/database';
import { AngularFireAuthModule} from 'angularfire2/auth';
import { AngularFireModule} from 'angularfire2';
import { environment} from '../environments/environment';
import { SoloPageComponent } from './solo-page/solo-page.component';
import { AuthComponent } from './auth/auth.component';
import { SignInComponent } from './auth/sign-in/sign-in.component';
import { SignUpComponent } from './auth/sign-up/sign-up.component';
import { AuthService} from './auth/auth.service';
import { FilterPipe } from './advert-page/advert-list/filterdata.pipe';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { NgxPaginationModule} from 'ngx-pagination';
import { NgxWebstorageModule} from 'ngx-webstorage';
import { SpinnerComponent } from './spinner/spinner.component';
import { AdvertListAdminComponent } from './advert-page/advert-list-admin/advert-list-admin.component';
import { AdvertListPersonalComponent } from './advert-page/advert-list-personal/advert-list-personal.component';
import { AuthGuard} from './auth/auth-guard.service';
import { HttpClient } from '@angular/common/http';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { LanguageService} from './language.service';


export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    routingComponents,
    CreateMainComponent,
    CreaterightComponent,
    AdvertListComponent,
    SoloPageComponent,
    AuthComponent,
    SignInComponent,
    SignUpComponent,
    FilterPipe,
    SpinnerComponent,
    AdvertListAdminComponent,
    AdvertListPersonalComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    AngularFireModule.initializeApp(environment.firebase),
    NgbModule,
    BsDatepickerModule.forRoot(),
    NgxPaginationModule,
    NgxWebstorageModule.forRoot(),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    BrowserAnimationsModule,

  ],
  providers: [FirebaseService, AuthService, AuthGuard, LanguageService],
  bootstrap: [AppComponent]
})
export class AppModule { }
