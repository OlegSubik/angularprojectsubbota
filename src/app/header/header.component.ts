import {Component, OnInit} from '@angular/core';
import {AuthService} from '../auth/auth.service';
import { TranslateService} from '@ngx-translate/core';
import {LanguageService} from '../language.service';


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor(private authService: AuthService,
              private translate: TranslateService,
              private lang: LanguageService
              ) {
    translate.setDefaultLang('en');
  }

  ngOnInit() {
  }

  onLogout() {
    this.authService.logout();
  }
  // switchLanguage(language: string) {
  //   this.translate.use(language);
  // }

}
