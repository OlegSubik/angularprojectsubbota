import {Pipe, PipeTransform} from '@angular/core';


@Pipe({
  name: 'filterdata'
})
export class FilterPipe implements PipeTransform {

  transform(items: any[], value: string): any[] {
    if (!items) return [];
    if (!value) return items;
    if (value === '' || value == null) return [];
    return items.filter(e => (
        e.fName.toLocaleLowerCase().includes(value.toLocaleLowerCase())
        || e.type.toLocaleLowerCase().includes(value.toLocaleLowerCase())
        || e.tag.toLocaleLowerCase().includes(value.toLocaleLowerCase())
        || e.postDate.toLocaleLowerCase().includes(value.toLocaleLowerCase())
        || e.title.toLocaleLowerCase().includes(value.toLocaleLowerCase())
        || e.description.toLocaleLowerCase().includes(value.toLocaleLowerCase())
      )
    );
  }

}
