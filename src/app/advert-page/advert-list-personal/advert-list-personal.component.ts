import {Component, OnInit} from '@angular/core';
import {FirebaseService} from '../../firebase.service';
import {Postinfo} from '../../create-page/create-main/formInfo.model';
import {Router} from '@angular/router';
import {AuthService} from '../../auth/auth.service';

@Component({
  selector: 'app-advert-list-personal',
  templateUrl: './advert-list-personal.component.html',
  styleUrls: ['./advert-list-personal.component.css']
})
export class AdvertListPersonalComponent implements OnInit {
  info: { key: string }[] = [new Postinfo(
    '',
    '',
    '',
    '',
    '',
    '',
    '',
    '',
    '',
    '',
    true)];

  private today;
  items: any;
  showSpinner = true;


  constructor(private FirebaseService: FirebaseService,
              private router: Router,
              private authService: AuthService) {
  }

  ngOnInit() {

    const today: any = new Date();
    let dd: any = today.getDate();
    let mm: any = today.getMonth() + 1; //January is 0!
    const yyyy: any = today.getFullYear();
    if (dd < 10) {
      dd = '0' + dd;
    }
    if (mm < 10) {
      mm = '0' + mm;
    }
    this.today = yyyy + '-' + mm + '-' + dd;


    // @ts-ignore
    this.items = this.FirebaseService.getPosts({limitToLast: 5});
    this.items.subscribe(() => this.showSpinner = false);

    this.FirebaseService.getPosts().subscribe(card => {
      this.info = card
        .filter(i => {
          return  i.email === this.authService.email;
        })
        .sort((a, b) => {
          if (a.postDate < b.postDate) {
            return 1;
          } else {
            return -1;
          }
        });
    });
  }

  sortDateDesc() {

    this.FirebaseService.getPosts().subscribe((card) => {
      this.info = card.sort((a, b) => {
        if (a.postDate > b.postDate) {
          return 1;
        } else {
          return -1;

        }
      });

    });
  }

  sortDateAsc() {
    this.FirebaseService.getPosts().subscribe((card) => {
      this.info = card.sort((a, b) => {
        console.log(card);
        if (a.postDate < b.postDate) {
          return 1;
        } else {
          return -1;
        }
      });
    });
  }
  hidePost(key, isVisible) {
    let switchIsVisible = !isVisible;
    this.FirebaseService.editPost(key, {isVisible: switchIsVisible});
  }
  photoError(event) {
    event.target.src = 'https://pngimage.net/wp-content/uploads/2018/06/not-found-png-3.png';
  }
}
