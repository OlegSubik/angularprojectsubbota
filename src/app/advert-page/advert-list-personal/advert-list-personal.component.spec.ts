import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdvertListPersonalComponent } from './advert-list-personal.component';

describe('AdvertListPersonalComponent', () => {
  let component: AdvertListPersonalComponent;
  let fixture: ComponentFixture<AdvertListPersonalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdvertListPersonalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdvertListPersonalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
