import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdvertListAdminComponent } from './advert-list-admin.component';

describe('AdvertListAdminComponent', () => {
  let component: AdvertListAdminComponent;
  let fixture: ComponentFixture<AdvertListAdminComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdvertListAdminComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdvertListAdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
