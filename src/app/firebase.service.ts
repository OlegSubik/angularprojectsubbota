import { Injectable } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';
import { AngularFireList } from 'angularfire2/database';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { AuthService } from './auth/auth.service';
import { HttpClient } from '@angular/common/http';


// if Mongo
/*interface PostInterface {
   fName: string;
   email: string;
   type: string;
   tag: string;
   postDate: string;
   title: string;
   description: string;
   img: string;
   key: string;
}*/


@Injectable()
export class FirebaseService {

  postRef: AngularFireList<any>;
  posts: Observable<any[]>;
  postinfo: object;

  constructor(private http: AngularFireDatabase, private authService: AuthService,
              private httpClient: HttpClient) {
  }


  // if Mongo
/*
  getPosts() {
    return this.httpClient.get('http://localhost:4100').pipe(
      map(post => {
        return (<PostInterface[]>post).map(el => {
          let oldPost = {...el}
          let k = oldPost['_id']
          oldPost.key = k
          delete oldPost['_id']
          return oldPost
        });
      }));
  }
*/

  //if Firebase
  getPosts() {
    this.postRef = this.http.list('/forms');
    return this.posts = this.postRef.snapshotChanges().pipe(
      map(changes => {
        return changes.map(c => ({
          key: c.payload.key, ...c.payload.val()
        }));
      })
    );
  }

  getPost(keyId) {
    return this.http.list(`/forms/` + keyId)
      .snapshotChanges()
      .pipe(map(items => {
        console.log(items);
        const dataItem = {};
        items.map(a => {
          const data = a.payload.val();
          const key = a.payload.key;
          dataItem[key] = data;
        });
        console.log('---------Post goes here----------', dataItem);
        return dataItem;
      }));
  }



sendPost(formValue) {
    // if Firbase
    return this.http.list('/forms').push(formValue);

  // if Mongo
  // return this.httpClient.post('http://localhost:4100/create', formValue).subscribe();
  }

  editPost(key, data) {
    return this.http.list('/forms').update(key, data);

  }

deletePost(postKey) {
    console.log(postKey.key);
    // if Firebase
    return this.http.list('/forms').remove(postKey.key);
    // if Mongo
    // return this.httpClient.delete('http://localhost:4100/create', postKey).subscribe();

  }


}

