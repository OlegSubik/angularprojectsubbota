import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AdvertPageComponent} from './advert-page/advert-page.component';
import {CreatePageComponent} from './create-page/create-page.component';
import {SoloPageComponent} from './solo-page/solo-page.component';
import {SignUpComponent} from './auth/sign-up/sign-up.component';
import {SignInComponent} from './auth/sign-in/sign-in.component';
import {AdvertListAdminComponent} from './advert-page/advert-list-admin/advert-list-admin.component';
import {AdvertListPersonalComponent} from './advert-page/advert-list-personal/advert-list-personal.component';
import {AuthGuard} from './auth/auth-guard.service';

const routes: Routes = [
  { path: 'advert-page', component: AdvertPageComponent},
  { path: 'admin-page', component: AdvertListAdminComponent, canActivate: [AuthGuard]},
  { path: 'personal-page', component: AdvertListPersonalComponent},
  { path: 'create-page', component: CreatePageComponent},
  { path: 'post-detail/:key', component: SoloPageComponent},
  { path: 'signup', component: SignUpComponent},
  { path: 'signin', component: SignInComponent},
  { path: '', component: AdvertPageComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

export const  routingComponents = [
  AdvertPageComponent,
  AdvertListAdminComponent,
  AdvertListPersonalComponent,
  CreatePageComponent,
  SoloPageComponent,
  SignUpComponent,
  SignInComponent];
