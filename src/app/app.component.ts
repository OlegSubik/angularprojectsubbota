import { Component, OnInit  } from '@angular/core';

import * as firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/firestore';
import 'firebase/app';
import { TranslateService} from '@ngx-translate/core';



@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],


})
export class AppComponent implements OnInit {




  constructor(private translate: TranslateService) {
    translate.setDefaultLang('en');
  }
  ngOnInit() {
    firebase.initializeApp({apiKey: 'AIzaSyDQf6Vd_mn_PM3RznbqPyDGZPAy9LrDvIo', authDomain: 'myangilarproject.firebaseapp.com', databaseURL: 'https://myangilarproject.firebaseio.com'});
  }


}
