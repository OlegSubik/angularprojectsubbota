import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SoloPageComponent } from './solo-page.component';

describe('SoloPageComponent', () => {
  let component: SoloPageComponent;
  let fixture: ComponentFixture<SoloPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SoloPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SoloPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
