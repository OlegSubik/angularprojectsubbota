import {Component, OnInit} from '@angular/core';
import {Postinfo} from '../create-page/create-main/formInfo.model';
import {FirebaseService} from '../firebase.service';
import {ActivatedRoute} from '@angular/router';
import {AuthService} from '../auth/auth.service';

@Component({
  selector: 'app-solo-page',
  templateUrl: './solo-page.component.html',
  styleUrls: ['./solo-page.component.css']
})
export class SoloPageComponent implements OnInit {

  private keyInfo;
  private description;
  private email;
  private fName;
  private postDate;
  private tag;
  private type;
  private title;
  private img;
  private expireDate;
  private isVisible: boolean;

  constructor(private FirebaseService: FirebaseService,
              private authService: AuthService,
              private route: ActivatedRoute) {
  }

  ngOnInit() {


    const id = this.route.snapshot.paramMap.get('key');
    this.keyInfo = id;
    this.FirebaseService.getPost(id).subscribe((post: Postinfo) => {
      this.description = post.description;
      this.email = post.email;
      this.fName = post.fName;
      this.postDate = post.postDate;
      this.tag = post.tag;
      this.title = post.title;
      this.type = post.type;
      this.img = post.img;
      this.expireDate = post.expireDate;
      this.isVisible = post.isVisible;
    });
  }

  photoError(event) {
    event.target.src = 'https://pngimage.net/wp-content/uploads/2018/06/not-found-png-3.png';
  }

  edit(fieldName, data) {
    console.log(data);
    this.FirebaseService.editPost(this.keyInfo, {[fieldName]: data});
  }
  editImage(value) {
    console.log(value);
    this.FirebaseService.editPost(this.keyInfo, {img: value});
  }

  hidePost(key, isVisible) {
    let switchIsVisible = !isVisible;
    this.FirebaseService.editPost(key, {isVisible: switchIsVisible});
  }

isOwner() {
    return this.email === this.authService.email;
}


}

