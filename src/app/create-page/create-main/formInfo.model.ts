export class Postinfo {
  // public $key: string;
  // public img: string;
  // public description: string;


  constructor(public fName: string,
              public email: string,
              public type: string,
              public tag: string,
              public postDate: string,
              public title: string,
              public description: string,
              public img: string,
              public key: string,
              public expireDate: string,
              public isVisible: boolean

  ) {
// this.description = description;
    // this.$key = key;
    // this.img = img;
  }
}

// export class Postinfo {
//   public fName: string;
//   public email: string;
//   public type: string;
//   public tag: string;
//   public postDate: string;
//   public title: string;
//   public description: string;
//   // public $key: string;
//   // public img: string;
//
//   constructor(fName: string, email: string, type: string, tag: string, postDate: string, title: string, description: string) {
//     this.fName = fName;
//     this.email = email;
//     this.type = type;
//     this.tag = tag;
//     this.postDate = postDate;
//     this.title = title;
//     this.description = description;
//     // this.$key = key;
//     // this.img = img;
//   }
// }

