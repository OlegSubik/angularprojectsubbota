import {Component, OnInit, Output} from '@angular/core';
import { Postinfo} from './formInfo.model';
import { FirebaseService } from '../../firebase.service';
import { AuthService} from '../../auth/auth.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-create-main',
  templateUrl: './create-main.component.html',
  styleUrls: ['./create-main.component.css']
})
export class CreateMainComponent implements OnInit {

  private  today;
  public minDate;
  private model;
  private success = false;
  info: Postinfo = new Postinfo(
    'Testname',
    'email@email.com',
    'News' ,
    '#qwe1' ,
    '',
    'Test title',
    'Test description Test description Test description Test description Test description Test description ',
    '',
    '',
    '',
    true);



  constructor(public FirebaseService: FirebaseService,
              private  authService: AuthService,
              private router: Router) {

  }

  ngOnInit() {

    const today: any = new Date();
    let dd: any = today.getDate();
    let mm: any = today.getMonth() + 1; //January is 0!
    const yyyy: any = today.getFullYear();
    this.minDate = {year: yyyy, month: mm, day: dd};

    if (dd < 10) {
      dd = '0' + dd;
    }
    if ( mm < 10) {
      mm = '0' + mm;
    }
    this.today = yyyy + '-' + mm + '-' + dd;


  }
  onSubmit(v) {
    let year = v.expireDate.year.toString();
    let month = v.expireDate.month.toString();
    if ( month < 10) {
      month = '0' + month;
    }
    let day = v.expireDate.day.toString();
    if (day < 10) {
      day = '0' + day;
    }
    v.expireDate = year + '-' + month + '-' + day;
    this.FirebaseService.sendPost(v);
    this.success = true;
    if (this.authService.isAdmin()) {
      setTimeout(() => this.router.navigate(['/admin-page']), 1500);
    } else {
      setTimeout(() => this.router.navigate(['/advert-page']), 1500);

    }
  }


}
