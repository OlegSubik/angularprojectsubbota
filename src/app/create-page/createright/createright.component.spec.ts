import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreaterightComponent } from './createright.component';

describe('CreaterightComponent', () => {
  let component: CreaterightComponent;
  let fixture: ComponentFixture<CreaterightComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreaterightComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreaterightComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
