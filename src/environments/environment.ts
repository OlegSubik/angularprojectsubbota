// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase : {
    apiKey: 'AIzaSyDQf6Vd_mn_PM3RznbqPyDGZPAy9LrDvIo',
    authDomain: 'myangilarproject.firebaseapp.com',
    databaseURL: 'https://myangilarproject.firebaseio.com',
    projectId: 'myangilarproject',
    storageBucket: 'myangilarproject.appspot.com',
    messagingSenderId: '1039147035658'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
